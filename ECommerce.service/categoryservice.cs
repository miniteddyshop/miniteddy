﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECommerce.Database;
using ECommerce.Entities;

namespace ECommerce.service
{
    public class categoryservice
    {
        public List<Category> GetCategory()
        {
            using (var context = new ECommerceContext())
            {
               return context.CategoryDB.ToList();
            }
        }
        public void SaveCategory(Category category)
        {
            using (var context = new ECommerceContext())
            {
                context.CategoryDB.Add(category);
                context.SaveChanges();
            } 
        }
        public Category GetCategory(int id)
        {
            using (var context = new ECommerceContext())
            {
             return context.CategoryDB.Find(id);       
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new ECommerceContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCategory(int id)
        {
            using (var context = new ECommerceContext())
            {
                var category= context.CategoryDB.Find(id);
                context.CategoryDB.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
