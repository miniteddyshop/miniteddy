﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECommerce.Entities;
using ECommerce.Database;

namespace ECommerce.service
{
    public class productservice
    {
        
        public List<Product> GetProduct()
        {
            using (var context = new ECommerceContext())
            {
                return context.ProductDB.ToList();
            }
        }
        public void ProductCreate(Product product)
        {
            using (var context = new ECommerceContext())
            {
                context.ProductDB.Add(product);
                context.SaveChanges();
            }
        }

        public Product GetProduct(int id)
        {
            using (var context = new ECommerceContext())
            {
                return context.ProductDB.Find(id);
            }
        }
        public void ProductUpdate(Product product)
        {
            using (var context = new ECommerceContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();  
            }
        }
        public void DeleteProduct(int id)
        {
            using (var context = new ECommerceContext())
            {
                var product =context.ProductDB.Find(id);
                context.ProductDB.Remove(product);
                context.SaveChanges();
            }
        }
    }
}
