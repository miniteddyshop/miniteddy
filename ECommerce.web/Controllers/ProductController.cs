﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECommerce.service;
using ECommerce.Entities;

namespace ECommerce.web.Controllers
{
    public class ProductController : Controller
    {
        productservice p = new productservice();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string search)
        {
            var product = p.GetProduct();
            if (string.IsNullOrEmpty(search) == false)
            {
                product = product.Where(x => x.Name != null && x.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            return PartialView(product);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Product product)
        {
            p.ProductCreate(product);
            return RedirectToAction("ProductTable");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var product = p.GetProduct(id);
            return PartialView(product);
        }
        [HttpPost]
        public ActionResult Edit(Product product)
        {
            p.ProductUpdate(product);
            return RedirectToAction("ProductTable");
        }

        //[HttpGet]
        //public ActionResult delete(int id)
        //{
        //    var product = p.GetProduct(id);
        //    return PartialView(product);
        //}
        [HttpPost]
        //public ActionResult delete(Product product)
        //{
        //    p.DeleteProduct(product.ID);
        //    return RedirectToAction("ProductTable");
        //}

        public ActionResult delete(int id)
        {
            p.DeleteProduct(id);
            return RedirectToAction("ProductTable");
        }
    }
}