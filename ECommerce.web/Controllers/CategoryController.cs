﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECommerce.service;
using ECommerce.Entities;

namespace ECommerce.web.Controllers
{
    public class CategoryController : Controller
    {
        categoryservice c = new categoryservice();

        public ActionResult Index()
        {
            var category = c.GetCategory();
            return View(category);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            c.SaveCategory(category);
            return RedirectToAction("Index");
        }
    
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category= c.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            c.UpdateCategory(category);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var category = c.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public ActionResult Delete(Category category)
        {
            
            c.DeleteCategory(category.ID);
            return RedirectToAction("Index");
        }
    }
}