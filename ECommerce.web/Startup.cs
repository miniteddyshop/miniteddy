﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECommerce.web.Startup))]
namespace ECommerce.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
