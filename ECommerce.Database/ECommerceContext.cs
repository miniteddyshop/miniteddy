﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ECommerce.Entities;

namespace ECommerce.Database
{
    public class ECommerceContext:DbContext
    {
        public ECommerceContext() : base("ECommerceConnection")
        { }

        public DbSet<Product> ProductDB { get; set; }
        public DbSet<Category> CategoryDB { get; set; }
       
    }
}
